require "mediawiki_api"
require "ffi-icu"
class Wiktionary
end
class Wiktionary::Thesaurus
  # Create the client to browse the wiktionnary
  # Note: only French version is taken into account so far
  def initialize
    version = 'fr'
    domaine = 'wiktionary.org'
    @client = MediawikiApi::Client.new "https://#{version}.#{domaine}/w/api.php"
  end

  # Return the wikicode of the thesaurus for the given +title+ in +language+
  def wikicode(title, language='français')
    (@client.get_wikitext "Thésaurus:#{title}/#{language}").body
  end

  # Return LSON hierachical representation of the`title` thesaurus
  def lson(title)
    section_depth = 0
    list_depth = 0
    thesaurus = ["local p = {}\np.thesaurus = {"]
    wikicode(title).each_line do |line|
      line = line.force_encoding('UTF-8')
      case line
      when /^{{/ # Template, ie. `{{(}}`
        next
      when /^\*/ # This is a list item
        label, depth = entry_extract(line)
        indentation = "\t" * (depth + section_depth)
        if depth > 1 && depth > list_depth
          # The current entry deepen the list, so the last entry is reformated
          # to become a key to which this depth of list is assigned
          thesaurus[-1].sub!(/"/, '["').sub!(/",/, '"] = {')
        elsif depth < list_depth
          # The current entry shallow the list, so close the previous list depth
          thesaurus.push %Q<\n#{indentation}},>
        end
        # Append the current item as simple string value
        thesaurus.push %Q<\n#{indentation}"#{label}",>
        list_depth = depth
      when /^=/ # This is a section title
        label, depth = section_extract(line)
        thesaurus.push add_section(label, depth, section_depth)
        section_depth = depth
      else # Misc. raw wikitext
        next
      end
    end
    section_depth.downto(1) do |depth|
      thesaurus.push "\n#{"\t" * depth}}," 
    end
    thesaurus.push "\n}" 
    return thesaurus.join
  end

  # Return a list of existing thesaurus
  def list
    body = (@client.get_wikitext "Thésaurus:Liste de tous les sujets").body
    body = body.force_encoding('UTF-8')
    titles = []
    body.each_line do |line|
      case line
      when /^\*/ # This is a list item
        titles.push line[/^\* \[\[Thésaurus:([^|]*)(|.*\]\])/,1]
      else
        next
      end
    end
    collator = ICU::Collation::Collator.new("fr")
    titles.uniq.compact.sort{ |a,b| collator.compare(a, b)}.join("\n")
  end

  private
  def entry_extract(line)
    depth = line[/^([#\*]+).*/,1].length
    label = line[/^([#\*]+)\s+\[\[([^\[\]]*)\]\]\s+$/,2] || line[/^([#\*]+)\s+(.*)/,2]
    return label, depth
  end

  def section_extract(line)
    depth = line[/^(=+).*/,1].length - 1
    label = line[/^(=+)\s+(.*)\s+=+/,2]
    return label, depth
  end


  # Return a string of lua code representing a thesaurus entry for the context
  # infered from parameters
  def add_entry(label, depth, previous_depth)
      indentation = "\t" * depth
      text = ''
      if depth <= previous_depth
        previous_depth.downto(depth) do |that_depth|
          text.concat "\n#{"\t" * that_depth}}," 
        end
      end
      text.concat %Q<\n#{indentation}["#{label}"] = {>
  end

  # Return a string of lua code representing a section for the context infered
  # from parameters
  def add_section(label, depth, previous_depth)
      indentation = "\t" * depth
      text = ''
      if depth <= previous_depth
        previous_depth.downto(depth) do |that_depth|
          text.concat "\n#{"\t" * that_depth}}," 
        end
      end
      text.concat %Q<\n#{indentation}["#{label}"] = {>
  end
end
